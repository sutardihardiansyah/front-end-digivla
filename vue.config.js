const fs = require('fs')
const packageJson = fs.readFileSync('./package.json')
const version = JSON.parse(packageJson).version || 0
const webpack = require('webpack')
module.exports = {
    configureWebpack: {
        plugins: [
            new webpack.DefinePlugin({
                'process.env': {
                    PACKAGE_VERSION: '"' + version + '"'
                }
            })
        ]
    },
    devServer: {
        // open: process.platform === 'darwin',
        // host: '0.0.0.0',
        port: 8080, // CHANGE YOUR PORT HERE!
        https: true,
        // hotOnly: false,
    },
}