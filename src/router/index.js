import Vue from "vue";
import VueRouter from "vue-router";
import Cookie from "vue-cookies";
import store from "../store/index.js";
import MainLayout from "../views/Layout/MainLayout.vue";
import Dashboard from "../views/Dashboard.vue";
import UnggahanTerbaru from "../views/UnggahanTerbaru.vue";
import Analisis from "../views/Analisis.vue";
import Influencer from "../views/Influencer.vue";
import TrashBin from "../views/TrashBin.vue";
import Maps from "../views/Map.vue";
import About from "../views/About.vue";
import ManajemenKeyword from "../views/ManajemenKeyword.vue";
import LogIn from "../views/SignIn.vue";
import Coba from "../views/UnggahanTerbaruOld.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "LogIn",
    component: LogIn,
    meta: {
      guest: true
    }
  },
  {
    path: "/",
    redirect: "dashboard",
    component: MainLayout,
    // meta: {
    //   auth: true,
    // },
    children: [{
        path: "dashboard",
        name: "Dashboard",
        component: Dashboard,
        meta: {
          auth: true
        }
      },
      {
        path: "UnggahanTerbaru",
        name: "Unggahan Terbaru",
        component: UnggahanTerbaru,
      },
      {
        path: "analisis",
        name: "Analisis",
        component: Analisis,
      },
      {
        path: "influencer",
        name: "Influencer",
        component: Influencer,
      },
      {
        path: "trash",
        name: "Trash",
        component: TrashBin,
      },
      {
        path: "map",
        name: "Map",
        component: Maps,
      },
      {
        path: "about",
        name: "About",
        component: About,
      },
      {
        path: "ManajemenKeyword",
        name: "Manajemen Keyword",
        component: ManajemenKeyword,
      },
      {
        path: "coba",
        name: "Coba",
        component: Coba,
      },
    ],
  },
  
];

const router = new VueRouter({
  routes,
  mode: "history",
  scrollBehavior (to) {
    
    if(to.path != '/influencer') {
      return { x: 0, y: 0 }
    }
  },
});

router.beforeEach((to, from, next) => {
  // console.log(routes)
  // if (to.matched.some((record) => record.meta.auth)) {

  //   if (Cookie.get('medmon_ref') == null) {
  //     next('/login');
  //   } else {
  //     next()
  //   }

  // } else if (to.matched.some((record) => record.meta.guest)) {
  //   if (Cookie.get('medmon_ref') == null) {
  //     next();
  //   } else {

  //     next()
  //   }
  // } else {
  //   next()
  // }

  if (to.matched.some(record => record.meta.auth)) {
  
    if(Cookie.get('role_id') == 1) {
      let ref = encodeURI(btoa("logout"));
      let teks = encodeURI(btoa("Username tidak terdaftar."));
      let title = encodeURI(btoa("Maaf!"));
      store.dispatch("logout").then(() => {
        router
          .push({
            path: "/login",
            query: {
              log: teks,
              ref: ref,
              log_t: title,
            },
          })
          .catch(() => {});
      });
      
      return
    }
    if (store.getters.isLoggedIn && store.getters.user) {
      next()
      return
    }
    next('/login')
  }
  
  if (to.matched.some(record => record.meta.guest)) {
    
    if (!store.getters.isLoggedIn) {
      next()
      return
    }
    next('/')
  }

  next()

});
export default router;