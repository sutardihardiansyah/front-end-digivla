import axios from "axios"
// import Cookie from "vue-cookie";
import Cookie from "vue-cookies";
// import firebase from 'firebase/app';
// import 'firebase/auth';
// import VueJwtDecode from 'vue-jwt-decode'
import jwt_decode from "jwt-decode";

import {
    setHeaderToken,
    removeHeaderToken
} from '../../utils/auth'
export default {
    state: {
        user: null,
        isLoggedIn: false,
        projects: null,
    },
    mutations: {
        set_user(state, data) {
            state.user = data,
            state.isLoggedIn = true
        },
        reset_user(state) {
            state.user = null,
                state.isLoggedIn = false
        },
        set_detail_project(state, data) {
            state.projects = data
        },

        reset_detail_project(state) {
            state.projects = null;
        }
    },
    getters: {
        isLoggedIn(state) {
            return state.isLoggedIn
            // console.log(state.isLoggedIn)
        },
        user:  state => state.user,
        project(state) {
            return state.projects
        },

        // project:  state => state.projects,
        
    },
    actions: {
        set_login({commit}) {
            return new Promise((resolve) => {
                commit('set_user', Cookie.get('user'))
                resolve()
            })
        },

        

        Login({
            // dispatch,
            commit
        }, data) {
            return new Promise((resolve, reject) => {
                let api = process.env.VUE_APP_API_URL;
                axios.post(api + 'login', data,{
                    responseType: "json"
                }).then(response => {
                    const token = response.data['access-token'];
                    // const refresh_token = VueJwtDecode.decode(response.data['refresh-token']).exp ;
                   
                //    console.log(jwt_decode(token));
                    const refresh_token = response.data['refresh-token'] ;
                    const users = response.data.user;
                    let decoded = jwt_decode(refresh_token);
                    let time = decoded.exp;
                    let role_id = jwt_decode(token).role
                    localStorage.setItem('users', response.data.user);
                    Cookie.set('user', users);
                    Cookie.set('role_id', role_id);
                    Cookie.set('medmon_ref', token);
                    Cookie.set('medmon_refresh', refresh_token, new Date(time * 1000));
                    Cookie.set('id_project', response.data.user.id_project);
                    localStorage.setItem('id_project', response.data.user.id_project)
                    // let id_project = response.data.user.id_project
                    setHeaderToken(token)
                    commit('set_user', users)
                    resolve(response)
                }).catch(err => {
                    commit('reset_user')
                    localStorage.removeItem('token')
                    reject(err)
                })
            })
        },

        async get_detail_project({commit}, data) {
            return new Promise((resolve, reject) => {
                let api = process.env.VUE_APP_API_URL + "project-detail/"+data.id_project;

                axios.get(api)
                .then(response => {
                    const project_detail = response.data.project_detail;
                    const tipe_project = response.data.project_detail.tipe;
                    Cookie.set('project_detail', project_detail);
                    Cookie.set('tipe_project', tipe_project);
                    commit('set_detail_project', project_detail)
                    commit('set_detail', project_detail)
                    resolve(response)
                }).catch(err => {
                    commit('reset_detail_project')
                    reject(err)
                })
                
            })
        },

        logout({
            commit
        }) {
            return new Promise((resolve) => {
                commit('reset_user')
                commit('reset_detail_project')
                commit('reset_detail')
                localStorage.removeItem('token')
                localStorage.removeItem('medmon_ref')
                localStorage.removeItem('medmon_refresh')
                localStorage.removeItem('token')
                localStorage.removeItem('id_project')
                localStorage.removeItem('user')
                // localStorage.removeItem('_value')
                localStorage.removeItem('grafiks')
                Cookie.remove('medmon_ref')
                Cookie.remove('medmon_refresh')
                Cookie.remove('id_project')
                Cookie.remove('user')
                Cookie.remove('us')
                Cookie.remove('user')
                Cookie.remove('role_id')
                Cookie.remove('project_detail')
                Cookie.remove('tipe_project')
                // firebase.auth().signOut()
                removeHeaderToken()
                resolve()
            })
        },

        async get_user({
            commit
        }) {
            if (!localStorage.getItem('token')) {
                return
            }
            try {
                // let response = await axios.get('user')
                commit('set_user', localStorage.getItem('user'))
                // console.log('okr')
            } catch (error) {
                commit('reset_user')
                commit('reset_detail_project')
                localStorage.removeItem('token')
                return error
            }
        }
    }
}