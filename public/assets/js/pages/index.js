﻿
$(function () {
    
});


function getChartJs(type) {
    var config = null;

    if (type === 'pie') {
        config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: [225, 500, 100],
                    backgroundColor: [
                        "rgb(160,214,117)",
                        "rgb(149,171,219)",
                        "rgb(229,125,119)"
                        
                    ],
                }],
                labels: [
                    "Positif",
                    "Netral",
                    "Negatif"
                ]
            },
            options: {
                responsive: true,
                legend: {
                    display: true,
                    position: 'bottom',
                }
            }
        }
    }else if (type === 'doughnut') {
        config = {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [25,40, 15, 20],
                    backgroundColor: [
                        "rgb(92,77,99)",
                        "rgb(109,134,135)",
                        "rgb(146,155,178)",
                        "rgb(235,152,78)"
                        
                    ],
                }],
                labels: [
                    "Kedaulatan NKRI",
                    "TKI",
                    "Isu Papua",
                    "Diplomasi"
                ]
            },
            options: {
                responsive: true,
                legend: {
                    display: true,
                    position: 'bottom',
                }
            }
        }
    }
    return config;
}
