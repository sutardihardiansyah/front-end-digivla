import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from "axios";
import store from './store'
// import Swal from 'sweetalert2'
import Cookie from "vue-cookies";
import VueSocketIO from 'vue-socket.io'
import Vuelidate from 'vuelidate'
import Toasted from 'vue-toasted';
import VueSimpleAlert from "vue-simple-alert";
import 'bootstrap';
import VueCarousel from 'vue-carousel';
import VueSweetalert2 from 'vue-sweetalert2';
import VueSlimScroll from 'vue-slimscroll'

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSlimScroll)
Vue.use(VueSweetalert2);
Vue.use(VueCarousel);
Vue.use(VueSimpleAlert);
Vue.use(Toasted)
Vue.use(Vuelidate) 
Vue.use(new VueSocketIO({
    debug: true,
    connection: 'https://kurasi.media:3003',
    vuex: {
        store,
        actionPrefix: 'SOCKET_',
        mutationPrefix: 'SOCKET_'
    },
    // options: { path: "/my-app/" } //Optional options
}))
Vue.config.productionTip = false
Vue.prototype.$axios = axios;
// Vue.prototype.$Swal = Swal;
Vue.prototype.$cookie = Cookie;



if(Cookie.get('medmon_refresh') ) {
  store.dispatch('set_login');
} 

    new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
